#!/bin/sh

# System Ruby (1.8.7)
yum install -y ruby ruby-devel rubygems

# Native dependencies for Deltacloud.
yum install -y gcc-c++ libxml libxml2-devel libxslt libxslt-devel sqlite sqlite-devel

# Install RBENV, RUBYBUILD and RUBY 1.9.2 for Deltacloud to install correctly.
yum install -y git
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
source ~/.bash_profile
rbenv install 1.9.2-p320
rbenv global 1.9.2-p320
rbenv rehash
gem install --no-ri --no-rdoc rake
ruby -v
gem -v

# Fetch Deltacloud core and client gems from rubygems, then install them.
gem fetch deltacloud-core
gem fetch deltacloud-client
gem install --no-ri --no-rdoc `ls -1 deltacloud-core*.gem`
gem install --no-ri --no-rdoc `ls -1 deltacloud-client*.gem`
rbenv rehash
echo "Drivers available with Deltacloud are:"
deltacloudd -l
echo "-------------------------"
echo "Type: 'deltacloudd -i mock' to run the mock driver"
echo "Type: 'deltacloudd -l' to see a list of available drivers"
echo "Type: 'deltacloudd -i ec2 -p 5000' to run specific driver on specified port"
echo "See: http://deltacloud.apache.org/run-deltacloud-server.html for further details."
echo "------------------------"
# Turn off firewall
service iptables stop
echo "Done."

