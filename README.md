Steps
=====

# On host machine
    vagrant up
    vagrant ssh

# On vagrant guest VM
    sudo -i -u root
    deltacloudd -i mock -p 8091

# On host machine
    open in browser: http://192.168.33.10:8091/api

